@extends("layouts.main")

@section("content")

<div class="container">
    <div class="border-slider"> 
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div id="main" role="main">
                    <section class="slider">
                        <div class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="http://localhost/project1/img/makeup.jpg"/></li>
                                <li><img class="img-responsive" src="http://localhost/project1/img/hairmodeling2.jpg"/></li>
                                <li><img class="img-responsive" src="http://localhost/project1/img/nails.jpg"/></li>
                                <li><img class="img-responsive" src="http://localhost/project1/img/asd.jpg"/></li>
                            </ul> <!-- slides -->

                        </div> <!-- flexslider -->
                    </section> <!-- slider -->
                </div> <!-- main -->

                <!-- jQuery -->
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
                <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

                <!-- FlexSlider -->
                <script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>

                <script type="text/javascript">
                    $(window).load(function(){
                        $('.flexslider').flexslider({
                            animation: "slide",
                            start: function(slider){
                                $('body').removeClass('loading');
                            }
                        });
                    });
                </script>
            </div>
        </div> <!-- row -->
    </div> <!-- border-slider -->
   

        @if (!Auth::guest() && Auth::user()->is_admin)       
            <a class="btn btn-default add-users-buttons" href="{{ route('beautyroom.create') }}">Add New Service</a>
            <a class="btn btn-default add-users-buttons" href="{{ route('user.index') }}">Users List</a>
        @else
        @endif
 


    <div class="services">
        <h6>Offers</h6>
        <div class="row">
            @foreach($services as $service)
               <div class="col-xs-12 col-md-3">
                    <div class="services-content service1">
                        <h2>{{ $service->title }}</h2>
                        <h5>{{ $service->price }}€</h5>
                        <a href="{{ route('beautyroom.show', $service->id) }}"><img src="{{ $service->image }}"></a>
                        
                        @if (!Auth::guest() && Auth::user()->is_admin)
                            <div class="row">
                                <div class="col-md-6 admin-buttons">
                                    <a class="btn btn-default" href="{{ route('beautyroom.edit', $service->id) }}">EDIT</a>
                                </div>

                                <div class="col-md-6 admin-buttons">
                                    <form method="POST" action="{{ route('beautyroom.destroy', $service->id) }}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        {{ csrf_field() }}
                                        <button class="btn btn-danger">DELETE</button>
                                    </form>
                                </div>
                            </div>
                        @else
                        @endif

                    </div>
                </div> 
            @endforeach
        </div> <!-- row -->
        
    </div> <!-- services -->

    <blockquote class="col-xs-12 col-sm-12 col-md-12 quote">
        <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.</p>
        <footer class="pull-right">Wikipedia</footer>
    </blockquote>

    <div class="details">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <h3 id="aboutUs-block">About Us</h3>
                <p>At Beauty Room, our quality trained Beauty Therapists consistently provide our clients with a high standard of customer service and skill.
                With one convenient location in the heart of Kaunas City, you can be sure to step out of the hustle and bustle and into our laid-back, relaxed and friendly environment.
                Escape to our fun & friendly environment where our fully qualified beauty therapists provide the highest standards, across our wide range of treatments.
                Seeking a quick wax on your lunch break, a wonderful make-up for your evening or another manicure and/or pedicure? We can cater for all your beauty needs.</p>
            </div>

        </div> <!-- row -->
    </div> <!-- details -->
</div> <!-- container -->

@endsection