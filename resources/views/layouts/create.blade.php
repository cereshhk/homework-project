@extends("layouts.main")

@section("content")

<!-- 	<h3>{{ isset($service) ? 'Edit service' : 'Add service' }}</h3>

	<form action="{{ route('beautyroom.store') }}" method="POST"> -->

<!-- <div id="forma">
    {{ Form::open(['url' => '/formos', 'method' => "POST", 'class' => 'labai-gera-forma']) }}
    {{ Form::text('name', 'this is name', ['class' => 'awesome', 'id'=>"geras"]) }}
    {{ Form::text('email', 'example@gmail.com', ['class' => 'awesome', 'id'=>"geras1"]) }}
    {{ Form::text('email-confirm', 'example@gmail.com', ['class' => 'awesome', 'id'=>"geras1"]) }}
    {{ Form::submit('Click Me!') }}
    {{ Form::close() }}
</div> -->

		 @if(isset($service))
			<form method="POST" action="{{ route('beautyroom.update', $service->id) }}">
			<input type="hidden" name="_method" value="PUT">
		@else
			<form method="POST" action="{{ route('beautyroom.store') }}">
		@endif

	<div class="container">
		@if(isset($service))
			<form method="POST" action="{{ route('beautyroom.update', $service->id) }}">
			<input type="hidden" name="_method" value="PUT">
		@else
			<form method="POST" action="{{ route('beautyroom.store') }}">
		@endif

		{{ csrf_field() }}

			<label>Title: </label>
			@if(isset($service))
				<input value="{{ $service['title'] }}" class="form-control" type="text" name="title">
			@else
				<input class="form-control" type="text" name="title">
			@endif


			<label>Price: </label>
			@if(isset($service))
				<input value="{{ $service['price'] }}" class="form-control" type="text" name="price" >
			@else
				<input class="form-control" type="text" name="price" >
			@endif

			<label>Image: </label>
			@if(isset($service))
				<input value="{{ $service['image'] }}" class="form-control" type="text" name="image" >
			@else
				<input class="form-control" type="text" name="image" >
			@endif



			<label>Description: </label>
			@if(isset($service))
				<textarea class="form-control" name="description" cols="30" rows="10" >{{ $service['description'] }}</textarea>
			@else
				<textarea class="form-control" name="description" cols="30" rows="10"></textarea>
			@endif

			<button class="btn btn-primary">Save</button>
			<!-- <input class="btn btn-primary" type="submit" name="save"> -->
		</form>
	</div>
@endsection