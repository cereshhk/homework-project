@extends("layouts.main")

@section("content")
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<h1>{{ $service->title }}</h1>
	            	<h2>{{ $service->price }} €</h2>
				</div>	
            </div>
            
            <div class="col-xs-12 col-md-6">
                <img class="img-responsive showblade-img" src="{{ $service->image }}">
            </div> 

           <div class="col-xs-12 col-md-6">
                <p>{{ $service->description }}</p>
            </div> 
		</div> <!-- row -->
	</div> <!-- container -->
@endsection