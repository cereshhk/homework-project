<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css"> -->
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="flexslider.css" type="text/css" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"> 
	<link rel="stylesheet" type="text/css" href="{{ asset('css/flexslider.css') }}">
	<!-- <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script> -->
	<title>Beauty salon page</title>
</head>
<body>
	<header>
		<div class="header-content">
			<div class="container">			
				<div class="row">	
					<div class="col-xs-6 col-md-6">
						<p><a href="{{ route('beautyroom.index') }}">www.beauty.com</a></p>
					</div>

					<div class="col-xs-6 col-md-6">
						@if (Auth::guest())
                            <div class="login-buttons"><a href="{{ url('login') }}">Staff Login</a></div>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
					</div>
				</div> <!-- row -->
			</div> <!-- container -->
		</div> <!-- header-content -->
	
		<div class="container">
			<div class="logo-nav">
				<div class="row">
					<div class="col-xs-12 col-md-6 logo"> <!-- col-xs-12 col-md-6 -->
						<a href="{{ route('beautyroom.index') }}"><img src="http://localhost/project1/img/logo1.png"></a>
					</div>

					<div class="col-xs-12 col-md-6 navi"> <!--  -->
						<ul>	
							<li><a href="{{ route('beautyroom.index') }}">Home</a></li>
							<li><a href="#aboutUs-block">About Us</a></li>
							<li><a href="{{ URL::to('contacts') }}">Contacts</a></li>
						</ul>
					</div>
				</div> <!-- row -->
			</div> <!-- logo-nav -->
		</div> <!-- container -->
	</header>
