@extends('layouts.main')

@section('content')
	<div class="container">
		<h3>Users List</h3> <br>
		<div class="row">
			<table class="col-md-12" width="100%">
				<tbody>
					<tr>
				    	<th>User Name</th>
				    	<th>User E-Mail</th>
				    	<th>Edit User</th>
				    	<th>Delete User</th>
			    	</tr>

				   	@foreach($users as $user)
			    		<tr>
				    		<td>{{ $user->name}}</td>
				    		<td>{{ $user->email }}</td>
				    		<td> 
					    		<a class="btn btn-primary" href="{{ route('user.edit', $user->id) }}">EDIT</a>
				    		</td>

				    		<td>
								<form method="POST" action="{{ route('user.destroy', $user->id) }}">
									<input type="hidden" name="_method" value="DELETE">
									{{ csrf_field() }}
									<button class="btn btn-danger">DELETE</button>
								</form>
							</td>
			    		</tr>
					@endforeach
				</tbody>
			</table>
		</div><!-- row -->
	</div>
@endsection

