@extends("layouts.main")

@section("content")
<div class="container">
	@if(isset($user))
		<form method="POST" action="{{ route('user.update', $user->id) }}">
		<input type="hidden" name="_method" value="PUT">
	@else
		<form method="POST" action="{{ route('user.store') }}">
	@endif



		{{ csrf_field() }}

		<label>Name: </label>
		@if(isset($user))
			<input value="{{ $user['name'] }}" class="form-control" type="text" name="name">
		@else
			<input class="form-control" type="text" name="name">
		@endif

		<label>Email: </label>
		@if(isset($user))
			<input value="{{ $user['email'] }}" class="form-control" type="text" name="email" >
		@else
			<input class="form-control" type="text" name="email" >
		@endif

		@if(isset($user))
			<button class="btn btn-primary">Update user</button>
		@else
			<button class="btn btn-primary">Add user</button>
		@endif

		@if($user->is_admin = 1)
			<input type="hidden" checked name="is_admin" value="0">
			<input type="checkbox" checked name="is_admin" value="1">Make this user Admin
		@else
		@endif
	</form>


</div>
@endsection    