@extends("layouts.main")

@section("content")
		<div class="container">
			<div class="row table">
				<div class="col-md-6">
					<p>
						<i class="fa fa-clock-o" aria-hidden="true"></i>
					</p>
					<h3>Opening hours</h3>	

					<table width="100%">
						<tbody>
							<tr>
								<th>Monday</th>
								<td>10am - 7pm</td>
							</tr>

							<tr>
								<th>Tuesday</th>
								<td>10am - 7pm</td>
							</tr>

							<tr>
								<th>Wednesday</th>
								<td>10am - 7pm</td>
							</tr>

							<tr>
								<th>Thursday</th>
								<td>10am - 7pm</td>
							</tr>

							<tr>
								<th>Friday</th>
								<td>10am - 7pm</td>
							</tr>

							<tr>
								<th>Saturday</th>
								<td>9am - 5pm</td>
							</tr>

							<tr>
								<th>Sunday</th>
								<td>Closed</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- col-md-6 -->

				<div class="col-md-6">
					<p>
						<i class="fa fa-map-marker" aria-hidden="true"></i>
					</p>
					<h3>Location</h3>	
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2294.526285093007!2d23.918840715710292!3d54.89368446538709!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e72270c7f424fb%3A0x6010bce82c1663a9!2sMi%C5%A1ko+g.%2C+Kaunas!5e0!3m2!1slt!2slt!4v1478675516526" class="map" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div> <!-- col-md-6 -->
			</div> <!-- row -->

			<div class="row contacts">
				<div class="col-md-4">
					<h3>Email</h3>
					<a href="#">lorem.info@lorem.ipsum</a>
				</div>

				<div class="col-md-4">
					<h3>Phone</h3>
					<a href="#">+37068451398</a>
				</div>

				<div class="col-md-4">
					<h3>Addres</h3>
					<a href="#">lorem Ipsumilion St. 9 - 2</a>
				</div>
			</div>

    <div class="details">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <h3 id="aboutUs-block">About Us</h3>
                <p>At Beauty Room, our quality trained Beauty Therapists consistently provide our clients with a high standard of customer service and skill.
                With one convenient location in the heart of Kaunas City, you can be sure to step out of the hustle and bustle and into our laid-back, relaxed and friendly environment.
                Escape to our fun & friendly environment where our fully qualified beauty therapists provide the highest standards, across our wide range of treatments.
                Seeking a quick wax on your lunch break, a wonderful make-up for your evening or another manicure and/or pedicure? We can cater for all your beauty needs.</p>
            </div>

        </div> <!-- row -->
    </div> <!-- details -->
</div>
@endsection