<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::resource('/', 'ServicesController@index');

Route::resource('/beautyroom', 'ServicesController');
Auth::routes();


// Route::get('/', function(){
// 	return redirect()->route('beautyroom.index');
// });
Route::get('/home', 'ServicesController@index');


Route::get('/contacts', 'ContactController@index');

Route::resource('/user', 'UserController');

Route::resource('/upload', 'UploadController@upload');

