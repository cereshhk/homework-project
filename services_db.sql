-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2016 at 09:31 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `services_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_11_14_160022_create_services_table', 1),
(4, '2016_11_18_104316_create_category_table', 2),
(5, '2016_11_19_115122_add_isadmin_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(3, 'Make-Up', 50, 'https://makeuponlineschool.com/wp-content/uploads/2015/11/Minneapolis-Makeup-Courses-.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur nibh ut bibendum facilisis. Proin volutpat massa tincidunt, viverra arcu nec, porta arcu. Quisque suscipit tristique condimentum. Aenean consequat suscipit mi et varius. Phasellus auctor finibus leo, et suscipit leo congue eget. Nunc sit amet porta metus. Cras ut dapibus est. Cras at libero nec metus egestas interdum. Donec ullamcorper sollicitudin hendrerit. Etiam mollis congue magna in pretium. Mauris ut bibendum orci. Mauris ut nulla vestibulum, lacinia mauris non, aliquam urna. Phasellus facilisis cursus feugiat.\r\nFrom 25 to 75€.\r\nFor more info - contact us!', '2016-11-16 04:23:12', '2016-12-07 04:55:10'),
(5, 'Hairs', 60, 'https://media1.britannica.com/eb-media/53/136653-004-E2E9D001.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur nibh ut bibendum facilisis. Proin volutpat massa tincidunt, viverra arcu nec, porta arcu. Quisque suscipit tristique condimentum. Aenean consequat suscipit mi et varius. Phasellus auctor finibus leo, et suscipit leo congue eget. Nunc sit amet porta metus. Cras ut dapibus est. Cras at libero nec metus egestas interdum. Donec ullamcorper sollicitudin hendrerit. Etiam mollis congue magna in pretium. Mauris ut bibendum orci. Mauris ut nulla vestibulum, lacinia mauris non, aliquam urna. Phasellus facilisis cursus feugiat.', '2016-11-17 14:05:21', '2016-11-21 05:17:55'),
(6, 'Manicure/Pedicure', 20, 'http://36qzne13engh1r8jdw3w4544.wpengine.netdna-cdn.com/wp-content/uploads/2010/05/manicure.jpg', 'Glow estheticians have the experience you are looking for to create your unique look. Our experience includes Boston Fashion week, trunk shows, runway and model work, magazine covers and more…\r\n\r\nWe can apply your Make-up for that special occasion or teach you how to create your new look. You have many options at Glow. We also sell an affordable cosmetic line that has professional staying power with a variety of options.', '2016-11-17 14:18:59', '2016-11-21 05:19:17'),
(9, 'Waxing', 70, 'http://evokesalonspa.com/wp-content/uploads/2014/10/Waxing-Evoke-Salon-Spa.jpg', 'Waxing is a great hair removal method for both male and female clientele. Waxing hair removes hair at the root and causes it to grow back much slower. Glow offers a full menu of waxing services for both men and women. Our estheticians are award winning for their speed waxing services and eye brow shaping. We use both honey wax and our special pain free blue wax, which reduces wax-related pain by shrink wrapping around the hair follicle and leaving the skin smooth. Plus, for those who are extra sensitive, Glow offers an exclusive Relax and Wax, which begins with an at home application of numbing cream to minimize pain.\r\n\r\nThere are, however, a few contraindications with waxing. We will not perform waxing services if you are sunburned, or are currently using Retin-A, Accutane, or products with Alpha hydroxy or Glycolic acid. Waxing appointments should not be scheduled on the same day as tanning appointments, body scrubs, or other treatments that may irritate the skin.', '2016-11-18 10:43:35', '2016-11-21 05:18:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_admin`) VALUES
(7, 'admin', 'admin@gmail.com', '$2y$10$o0u4o3h8AHD7R.6uPnhApuYNWSadXhpLXArMCFPfBVb4Q4sfrI3sK', '9zXtyzZVkwcdQpuXergbyYZnDXea2CFM2usG5TcNzyaOAosRfn9UfHDUuHWx', '2016-11-21 03:04:33', '2016-12-07 09:35:24', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
